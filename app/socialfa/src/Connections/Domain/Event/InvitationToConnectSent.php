<?php
declare(strict_types=1);

namespace App\Connections\Domain\Event;

use App\Connections\Domain\ValueObject\InvitationStatus;
use App\Connections\Domain\ValueObject\Message;
use App\Core\Domain\ValueObject\Id;
use Prooph\EventSourcing\AggregateChanged;

class InvitationToConnectSent extends AggregateChanged
{
    public function getId(): Id
    {
        return Id::fromUuid($this->payload['id']);
    }
    public function getInvitingUserId(): Id
    {
        return Id::fromUuid($this->payload['invitingUserId']);
    }
    public function getInvitedUserId(): Id
    {
        return Id::fromUuid($this->payload['invitedUserId']);
    }
    public function getMessage(): Message
    {
        return Message::createFromText($this->payload['message']);
    }
    public function getStatus(): InvitationStatus
    {
        return InvitationStatus::create($this->payload['status']);
    }
}
