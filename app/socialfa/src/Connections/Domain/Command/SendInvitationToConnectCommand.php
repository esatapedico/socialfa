<?php
declare(strict_types=1);

namespace App\Connections\Domain\Command;

class SendInvitationToConnectCommand
{
    /** @var string */
    private $invitationId;

    /** @var string */
    private $invitingUserId;

    /** @var string */
    private $invitedUserId;

    /** @var string */
    private $message;

    public function __construct(string $invitationId, string $invitingUserId, string $invitedUserId, string $message)
    {
        $this->invitationId   = $invitationId;
        $this->invitingUserId = $invitingUserId;
        $this->invitedUserId  = $invitedUserId;
        $this->message        = $message;
    }

    public function getInvitationId(): string
    {
        return $this->invitationId;
    }

    public function getInvitingUserId(): string
    {
        return $this->invitingUserId;
    }

    public function getInvitedUserId(): string
    {
        return $this->invitedUserId;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
