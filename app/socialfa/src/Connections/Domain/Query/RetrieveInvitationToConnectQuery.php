<?php
declare(strict_types=1);

namespace App\Connections\Domain\Query;

class RetrieveInvitationToConnectQuery
{
    /** @var string */
    private $invitationId;

    public function __construct(string $invitationId)
    {
        $this->invitationId = $invitationId;
    }

    public function getInvitationId(): string
    {
        return $this->invitationId;
    }
}
