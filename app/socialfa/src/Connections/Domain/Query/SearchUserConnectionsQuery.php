<?php
declare(strict_types=1);

namespace App\Connections\Domain\Query;

class SearchUserConnectionsQuery
{
    /** @var string $userId */
    private $userId;

    /** @var string */
    private $searchTerms;

    public function __construct(string $userId, string $searchTerms)
    {
        $this->userId = $userId;
        $this->searchTerms = $searchTerms;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getSearchTerms(): string
    {
        return $this->searchTerms;
    }
}
