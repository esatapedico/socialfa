<?php
declare(strict_types=1);

namespace App\Connections\Domain\Exception;

use DomainException;

class InvalidMessageException extends DomainException
{
    public static function tooShort(int $actual, int $expected): self
    {
        return new self("Message too short ({$actual} character(s)). Minimum: {$expected}");
    }

    public static function tooLong(int $actual, int $expected): self
    {
        return new self("Message too long ({$actual} character(s)). Maximum: {$expected}");
    }
}
