<?php
declare(strict_types=1);

namespace App\Connections\Domain\Exception;

use DomainException;

class InvalidInvitationStatusException extends DomainException
{
}
