<?php
declare(strict_types=1);

namespace App\Connections\Domain\Exception;

use DomainException;

class InvalidSearchTermException extends DomainException
{
    public static function tooLong(int $actual, int $expected): self
    {
        return new self("Search terms are too long ({$actual} character(s)). Maximum: {$expected}");
    }
}
