<?php
declare(strict_types=1);

namespace App\Connections\Domain\Service;

use App\Connections\Domain\Query\SearchAllUsersQuery;
use App\Connections\Domain\Query\RetrieveUserConnectionsQuery;
use App\Connections\Domain\ValueObject\SearchTerm;
use App\Connections\Infrastructure\Repository\UserConnectionRepositoryInterface;
use App\Connections\Presentation\UserConnectionsTransformerInterface;
use App\Core\Domain\ValueObject\Id;
use React\Promise\Deferred;

class SearchAllUsersService
{
    /** @var UserConnectionRepositoryInterface */
    private $userConnectionRepository;

    /** @var UserConnectionsTransformerInterface */
    private $userConnectionsTransformer;

    public function __construct(
        UserConnectionRepositoryInterface $userConnectionRepository,
        UserConnectionsTransformerInterface $userConnectionsTransformer
    )
    {
        $this->userConnectionRepository = $userConnectionRepository;
        $this->userConnectionsTransformer = $userConnectionsTransformer;
    }

    public function __invoke(SearchAllUsersQuery $query, Deferred $deferred)
    {
        $connections = $this->userConnectionRepository->searchAllByTerm(SearchTerm::fromString($query->getTerm()));
        $result = $this->userConnectionsTransformer->transform($connections);

        $deferred->resolve($result);
    }
}
