<?php
declare(strict_types=1);

namespace App\Connections\Domain\Service;

use App\Connections\Domain\Command\SendInvitationToConnectCommand;
use App\Connections\Domain\Exception\InvitationAlreadySentException;
use App\Connections\Domain\Model\Invitation;
use App\Connections\Domain\ValueObject\Message;
use App\Connections\Infrastructure\Repository\InvitationRepositoryInterface;
use App\Core\Domain\ValueObject\Id;

class InviteToConnectService
{
    /** @var InvitationRepositoryInterface */
    private $invitationRepository;

    public function __construct(InvitationRepositoryInterface $invitationRepository)
    {
        $this->invitationRepository = $invitationRepository;
    }

    public function __invoke(SendInvitationToConnectCommand $command)
    {
        $invitingUserId = Id::fromUuid($command->getInvitingUserId());
        $invitedUserId = Id::fromUuid($command->getInvitedUserId());

        $invitation = null;

        if (null !== $invitation) {
            throw new InvitationAlreadySentException('An invitation has already been sent between the users.');
        }

        $invitation = Invitation::send(
            Id::fromUuid($command->getInvitationId()),
            $invitingUserId,
            $invitedUserId,
            Message::createFromText($command->getMessage())
        );

        $this->invitationRepository->save($invitation);
    }
}
