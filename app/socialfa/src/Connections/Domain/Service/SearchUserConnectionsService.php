<?php
declare(strict_types=1);

namespace App\Connections\Domain\Service;

use App\Connections\Domain\Query\SearchUserConnectionsQuery;
use App\Connections\Domain\ValueObject\SearchTerm;
use App\Connections\Infrastructure\Repository\UserConnectionRepositoryInterface;
use App\Connections\Presentation\UserConnectionsTransformerInterface;
use App\Core\Domain\ValueObject\Id;
use React\Promise\Deferred;

class SearchUserConnectionsService
{
    /** @var UserConnectionRepositoryInterface */
    private $userConnectionRepository;

    /** @var UserConnectionsTransformerInterface */
    private $userConnectionsTransformer;

    public function __construct(
        UserConnectionRepositoryInterface $userConnectionRepository,
        UserConnectionsTransformerInterface $userConnectionsTransformer
    )
    {
        $this->userConnectionRepository = $userConnectionRepository;
        $this->userConnectionsTransformer = $userConnectionsTransformer;
    }

    public function __invoke(SearchUserConnectionsQuery $query, Deferred $deferred)
    {
        $connections = $this->userConnectionRepository->searchAllFromUserByTerm(
            Id::fromUuid($query->getUserId()),
            SearchTerm::fromString($query->getSearchTerms())
        );
        $result = $this->userConnectionsTransformer->transform($connections);

        $deferred->resolve($result);
    }
}
