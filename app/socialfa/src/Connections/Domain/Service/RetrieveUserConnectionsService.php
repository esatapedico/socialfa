<?php
declare(strict_types=1);

namespace App\Connections\Domain\Service;

use App\Connections\Domain\Query\RetrieveUserConnectionsQuery;
use App\Connections\Infrastructure\Repository\UserConnectionRepositoryInterface;
use App\Connections\Presentation\UserConnectionsTransformerInterface;
use App\Core\Domain\ValueObject\Id;
use React\Promise\Deferred;

class RetrieveUserConnectionsService
{
    /** @var UserConnectionRepositoryInterface */
    private $userConnectionRepository;

    /** @var UserConnectionsTransformerInterface */
    private $userConnectionsTransformer;

    public function __construct(
        UserConnectionRepositoryInterface $userConnectionRepository,
        UserConnectionsTransformerInterface $userConnectionsTransformer
    )
    {
        $this->userConnectionRepository = $userConnectionRepository;
        $this->userConnectionsTransformer = $userConnectionsTransformer;
    }

    public function __invoke(RetrieveUserConnectionsQuery $query, Deferred $deferred)
    {
        $connections = $this->userConnectionRepository->allFromUser(Id::fromUuid($query->getUserId()));
        $result = $this->userConnectionsTransformer->transform($connections);

        $deferred->resolve($result);
    }
}
