<?php
declare(strict_types=1);

namespace App\Connections\Domain\Service;

use App\Connections\Domain\Query\RetrieveInvitationToConnectQuery;
use App\Connections\Infrastructure\Repository\InvitationRepositoryInterface;
use App\Connections\Presentation\InvitationTransformerInterface;
use App\Core\Domain\ValueObject\Id;
use React\Promise\Deferred;

class RetrieveInvitationToConnectService
{
    /** @var InvitationRepositoryInterface */
    private $invitationRepository;

    /** @var InvitationTransformerInterface */
    private $invitationTransformer;

    public function __construct(
        InvitationRepositoryInterface $invitationRepository,
        InvitationTransformerInterface $invitationTransformer
    ) {
        $this->invitationRepository = $invitationRepository;
        $this->invitationTransformer = $invitationTransformer;
    }

    public function __invoke(RetrieveInvitationToConnectQuery $query, Deferred $deferred)
    {
        $invitation = $this->invitationRepository->byId(Id::fromUuid($query->getInvitationId()));
        $result = $this->invitationTransformer->transform($invitation);

        $deferred->resolve($result);
    }
}
