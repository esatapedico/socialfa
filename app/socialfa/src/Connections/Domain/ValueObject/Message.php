<?php
declare(strict_types=1);

namespace App\Connections\Domain\ValueObject;

use App\Connections\Domain\Exception\InvalidMessageException;

class Message
{
    /** @var int */
    private const MINIMUM_LENGTH = 2;

    /** @var int */
    private const MAXIMUM_LENGTH = 255;

    /** @var string */
    private $message;

    private function __construct(string $message)
    {
        $this->message = $message;
    }

    /**
     * @param string $text
     *
     * @return Message
     * @throws InvalidMessageException
     */
    public static function createFromText(string $text): self
    {
        $sanitizedMessage = filter_var($text, FILTER_SANITIZE_MAGIC_QUOTES);
        $length = strlen($sanitizedMessage);

        if ($length < self::MINIMUM_LENGTH) {
            throw InvalidMessageException::tooShort($length, self::MINIMUM_LENGTH);
        }

        if ($length > self::MAXIMUM_LENGTH) {
            throw InvalidMessageException::tooLong($length, self::MAXIMUM_LENGTH);
        }

        return new self($sanitizedMessage);
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
