<?php
declare(strict_types=1);

namespace App\Connections\Domain\ValueObject;

use App\Connections\Domain\Exception\InvalidInvitationStatusException;

class InvitationStatus
{
    /** @var string */
    public const SENT = 'SENT';

    /** @var string */
    public const RECEIVED = 'RECEIVED';

    /** @var string */
    public const ACCEPTED = 'ACCEPTED';

    /** @var string */
    public const REJECTED = 'REJECTED';

    /** @var string[] */
    public const VALID_STATUSES = [
        self::SENT,
        self::RECEIVED,
        self::ACCEPTED,
        self::REJECTED
    ];

    /** @var string */
    private $status;

    /**
     * InvitationStatus constructor.
     *
     * @param $status
     */
    private function __construct($status)
    {
        $this->status = $status;
    }

    public static function create(string $status)
    {
        if (!in_array($status, self::VALID_STATUSES)) {
            throw new InvalidInvitationStatusException("'{$status}' is not a valid invitation status.");
        }

        return new self($status);
    }

    /**
     * @return string
     */
    public function asString(): string
    {
        return $this->status;
    }
}
