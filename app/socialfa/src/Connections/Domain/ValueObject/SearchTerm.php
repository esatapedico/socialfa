<?php
declare(strict_types=1);

namespace App\Connections\Domain\ValueObject;

use App\Connections\Domain\Exception\InvalidSearchTermException;

class SearchTerm
{
    /** @var int */
    private const MAXIMUM_LENGTH = 30;

    /** @var string */
    private $term;

    private function __construct(string $term)
    {
        $this->term = $term;
    }

    public static function fromString(string $term): self
    {
        $sanitizedTerm = filter_var($term, FILTER_SANITIZE_STRING);
        $length = strlen($sanitizedTerm);

        if ($length > self::MAXIMUM_LENGTH) {
            throw InvalidSearchTermException::tooLong($length, self::MAXIMUM_LENGTH);
        }

        return new self($sanitizedTerm);
    }

    public function asString(): string
    {
        return $this->term;
    }
}
