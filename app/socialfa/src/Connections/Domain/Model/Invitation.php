<?php
declare(strict_types=1);

namespace App\Connections\Domain\Model;

use App\Connections\Domain\Event\InvitationToConnectSent;
use App\Connections\Domain\ValueObject\InvitationStatus;
use App\Connections\Domain\ValueObject\Message;
use App\Core\Domain\ValueObject\Id;
use JsonSerializable;
use Prooph\EventSourcing\AggregateChanged;
use Prooph\EventSourcing\AggregateRoot;

class Invitation extends AggregateRoot implements JsonSerializable
{
    /** @var Id */
    private $id;

    /** @var Id */
    private $invitingUserId;

    /** @var Id */
    private $invitedUserId;

    /** @var Message */
    private $message;

    /** @var InvitationStatus */
    private $status;

    protected function aggregateId(): string
    {
        return $this->id->asString();
    }

    protected function __construct()
    {
    }

    public static function send(
        Id $id,
        Id $invitingUserId,
        Id $invitedUserId,
        Message $message
    ): self
    {
        $invitation = new self();

        $invitation->recordThat(InvitationToConnectSent::occur(
            $id->asString(),
            [
                'id' => $id->asString(),
                'invitingUserId' => $invitingUserId->asString(),
                'invitedUserId' => $invitedUserId->asString(),
                'message' => $message->getMessage(),
                'status' => InvitationStatus::SENT
            ]
        ));

        return $invitation;
    }

    protected function apply(AggregateChanged $event): void
    {
        switch (get_class($event)) {
            case InvitationToConnectSent::class:
                $this->applyInvitationToConnectSent($event);
        }
    }

    private function applyInvitationToConnectSent(InvitationToConnectSent $event)
    {
        $this->id = $event->getId();
        $this->invitingUserId = $event->getInvitingUserId();
        $this->invitedUserId = $event->getInvitedUserId();
        $this->message = $event->getMessage();
        $this->status = $event->getStatus();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id->asString(),
            'invitingUserId' => $this->invitingUserId->asString(),
            'invitedUserId' => $this->invitedUserId->asString(),
            'message' => $this->message->getMessage(),
            'status' => $this->status->asString()
        ];
    }
}
