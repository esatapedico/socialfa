<?php
declare(strict_types=1);

namespace App\Connections\Domain\Model;

use App\Core\Domain\ValueObject\Id;
use App\Core\Domain\ValueObject\Name;
use App\Core\Domain\ValueObject\Username;
use App\Core\Domain\ValueObject\Email;
use DateTimeInterface;
use JsonSerializable;

class UserConnection implements JsonSerializable
{
    /** @var Id */
    private $id;

    /** @var Username */
    private $username;

    /** @var Name */
    private $firstName;

    /** @var Name */
    private $lastName;

    /** @var Email */
    private $email;

    /** @var DateTimeInterface */
    private $birthDate;

    public function __construct(
        Id $id,
        Username $username,
        Name $firstName,
        Name $lastName,
        Email $email,
        DateTimeInterface $birthDate
    ) {
        $this->id        = $id;
        $this->username  = $username;
        $this->firstName = $firstName;
        $this->lastName  = $lastName;
        $this->email     = $email;
        $this->birthDate = $birthDate;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id->asString(),
            'username' => $this->username->getUsername(),
            'firstName' => $this->firstName->getName(),
            'lastName' => $this->lastName->getName(),
            'email' => $this->email->getEmail(),
            'birthDate' => $this->birthDate->format('Y-m-d')
        ];
    }
}
