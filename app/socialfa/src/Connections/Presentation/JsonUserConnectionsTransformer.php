<?php
declare(strict_types=1);

namespace App\Connections\Presentation;

use App\Connections\Domain\Model\UserConnection;

class JsonUserConnectionsTransformer implements UserConnectionsTransformerInterface
{
    /**
     * @param UserConnection[] $connections
     *
     * @return mixed
     */
    public function transform(array $connections)
    {
        return json_encode($connections);
    }
}
