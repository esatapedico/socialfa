<?php
declare(strict_types=1);

namespace App\Connections\Presentation;

use App\Connections\Domain\Model\Invitation;

class JsonInvitationTransformer implements InvitationTransformerInterface
{
    /**
     * @param Invitation $invitation
     *
     * @return mixed
     */
    public function transform(Invitation $invitation)
    {
        return json_encode($invitation);
    }
}
