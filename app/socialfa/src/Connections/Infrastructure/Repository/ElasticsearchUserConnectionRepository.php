<?php
declare(strict_types=1);

namespace App\Connections\Infrastructure\Repository;

use App\Connections\Domain\Model\UserConnection;
use App\Connections\Domain\ValueObject\SearchTerm;
use App\Core\Domain\ValueObject\Email;
use App\Core\Domain\ValueObject\Id;
use App\Core\Domain\ValueObject\Name;
use App\Core\Domain\ValueObject\Username;
use DateTimeImmutable;
use Elastica\Index;
use Elastica\Result;

class ElasticsearchUserConnectionRepository implements UserConnectionRepositoryInterface
{
    /** @var Index */
    private $elasticSearchIndex;

    public function __construct(Index $elasticSearchIndex)
    {
        $this->elasticSearchIndex = $elasticSearchIndex;
    }

    /**
     * @param Id $userId
     *
     * @return UserConnection[]
     */
    public function allFromUser(Id $userId): array
    {
        $searchResults = $this->elasticSearchIndex->search(
            [
                'query' => [
                    'bool' => [
                        'filter' => [
                            [
                                'match' => [
                                    'connections' => $userId->asString()
                                ]
                            ]
                        ]
                    ]
                ]

        ]);

        if (empty($searchResults->getResults())) {
            return [];
        }

        return array_map(function (Result $userResult) {
            $data = $userResult->getSource();

            return new UserConnection(
                Id::fromUuid($userResult->getId()),
                Username::create($data['username']),
                Name::fromWord($data['firstName']),
                Name::fromWord($data['lastName']),
                Email::create($data['email']),
                DateTimeImmutable::createFromFormat('Y-m-d', $data['birthDate'])
            );
        }, $searchResults->getResults());
    }

    /**
     * @param Id $userId
     *
     * @param SearchTerm $term
     *
     * @return UserConnection[]
     */
    public function searchAllFromUserByTerm(Id $userId, SearchTerm $term): array
    {
        $query = [
            'query' => [
                'bool' => [
                    'filter' => [
                        [
                            'match' => [
                                'connections' => $userId->asString()
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if ($term->asString() !== '') {
            $query['query']['bool']['filter'][] = [
                'multi_match' => [
                    'query'  => $term->asString(),
                    'fields' => [
                        'username',
                        'firstName',
                        'lastName',
                        'email'
                    ]
                ]
            ];
        }

        $searchResults = $this->elasticSearchIndex->search($query);

        if (empty($searchResults->getResults())) {
            return [];
        }

        return array_map(function (Result $userResult) {
            $data = $userResult->getSource();

            return new UserConnection(
                Id::fromUuid($userResult->getId()),
                Username::create($data['username']),
                Name::fromWord($data['firstName']),
                Name::fromWord($data['lastName']),
                Email::create($data['email']),
                DateTimeImmutable::createFromFormat('Y-m-d', $data['birthDate'])
            );
        }, $searchResults->getResults());
    }

    /**
     * @param SearchTerm $term
     *
     * @return UserConnection[]
     */
    public function searchAllByTerm(SearchTerm $term): array
    {
        if ($term->asString() === '') {
            return $this->getAll();
        }

        $searchResults = $this->elasticSearchIndex->search(
            [
                'query' => [
                    'bool' => [
                        'filter' => [
                            [
                                'multi_match' => [
                                    'query'  => $term->asString(),
                                    'fields' => [
                                        'username',
                                        'firstName',
                                        'lastName',
                                        'email'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]);

        if (empty($searchResults->getResults())) {
            return [];
        }

        return array_map(function (Result $userResult) {
            $data = $userResult->getSource();

            return new UserConnection(
                Id::fromUuid($userResult->getId()),
                Username::create($data['username']),
                Name::fromWord($data['firstName']),
                Name::fromWord($data['lastName']),
                Email::create($data['email']),
                DateTimeImmutable::createFromFormat('Y-m-d', $data['birthDate'])
            );
        }, $searchResults->getResults());
    }

    /**
     * @return UserConnection[]
     */
    public function getAll(): array
    {
        $searchResults = $this->elasticSearchIndex->search();

        if (empty($searchResults->getResults())) {
            return [];
        }

        return array_map(function (Result $userResult) {
            $data = $userResult->getSource();

            return new UserConnection(
                Id::fromUuid($userResult->getId()),
                Username::create($data['username']),
                Name::fromWord($data['firstName']),
                Name::fromWord($data['lastName']),
                Email::create($data['email']),
                DateTimeImmutable::createFromFormat('Y-m-d', $data['birthDate'])
            );
        }, $searchResults->getResults());    }
}
