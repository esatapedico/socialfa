<?php
declare(strict_types=1);

namespace App\Connections\Infrastructure\Repository;

use App\Connections\Domain\Model\UserConnection;
use App\Connections\Domain\ValueObject\SearchTerm;
use App\Core\Domain\ValueObject\Id;

interface UserConnectionRepositoryInterface
{
    /**
     * @param Id $userId
     *
     * @return UserConnection[]
     */
    public function allFromUser(Id $userId): array;

    /**
     * @param Id $userId
     * @param SearchTerm $term
     *
     * @return UserConnection[]
     */
    public function searchAllFromUserByTerm(Id $userId, SearchTerm $term): array;

    /**
     * @param SearchTerm $term
     *
     * @return UserConnection[]
     */
    public function searchAllByTerm(SearchTerm $term): array;

    /**
     * @return UserConnection[]
     */
    public function getAll(): array;
}
