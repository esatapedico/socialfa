<?php
declare(strict_types=1);

namespace App\Connections\Infrastructure\Repository\EventStore;

use App\Connections\Domain\Model\Invitation;
use App\Connections\Infrastructure\Repository\InvitationRepositoryInterface;
use App\Core\Domain\ValueObject\Id;
use Prooph\EventSourcing\Aggregate\AggregateRepository;

class EventStoreInvitationRepository extends AggregateRepository implements InvitationRepositoryInterface
{
    public function save(Invitation $invitation): void
    {
        $this->saveAggregateRoot($invitation);
    }

    public function byId(Id $id): Invitation
    {
        return $this->getAggregateRoot($id->asString());
    }
}
