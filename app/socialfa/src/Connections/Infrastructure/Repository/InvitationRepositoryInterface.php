<?php
declare(strict_types=1);

namespace App\Connections\Infrastructure\Repository;

use App\Connections\Domain\Model\Invitation;
use App\Core\Domain\ValueObject\Id;

interface InvitationRepositoryInterface
{
    public function save(Invitation $invitation): void;

    public function byId(Id $id): Invitation;
}
