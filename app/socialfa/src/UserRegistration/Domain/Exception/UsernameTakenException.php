<?php
declare(strict_types=1);

namespace App\UserRegistration\Domain\Exception;

use DomainException;

class UsernameTakenException extends DomainException
{
}
