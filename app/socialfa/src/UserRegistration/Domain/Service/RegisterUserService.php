<?php
declare(strict_types=1);

namespace App\UserRegistration\Domain\Service;

use App\Core\Domain\ValueObject\Username;
use App\UserRegistration\Domain\Command\RegisterUserCommand;
use App\UserRegistration\Domain\Exception\UsernameTakenException;
use App\UserRegistration\Domain\Model\User;
use App\Core\Domain\ValueObject\Email;
use App\Core\Domain\ValueObject\Name;
use App\Core\Domain\ValueObject\Id;
use App\UserRegistration\Infrastructure\Repository\UserRepositoryInterface;
use DateTimeImmutable;

class RegisterUserService
{
    /** @var UserRepositoryInterface */
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(RegisterUserCommand $command)
    {
//        $user = $this->userRepository->byUsername($command->getUsername());
        $user = null;
        if (null !== $user) {
            throw new UsernameTakenException("Username {$command->getUsername()} already taken.");
        }

        $user = User::register(
            Id::fromUuid($command->getUserId()),
            Username::create($command->getUsername()),
            Name::fromWord($command->getFirstName()),
            Name::fromWord($command->getLastName()),
            Email::create($command->getEmail()),
            DateTimeImmutable::createFromFormat('Y-m-d', $command->getBirthDate())
        );

        $this->userRepository->save($user);
    }
}
