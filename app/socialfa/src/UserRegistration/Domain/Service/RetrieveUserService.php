<?php
declare(strict_types=1);

namespace App\UserRegistration\Domain\Service;

use App\Core\Domain\ValueObject\Id;
use App\UserRegistration\Domain\Query\RetrieveUserQuery;
use App\UserRegistration\Infrastructure\Repository\UserRepositoryInterface;
use App\UserRegistration\Presentation\UserTransformerInterface;
use React\Promise\Deferred;

class RetrieveUserService
{
    /** @var UserRepositoryInterface */
    private $userRepository;

    /** @var UserTransformerInterface */
    private $userTransformer;

    public function __construct(UserRepositoryInterface $userRepository, UserTransformerInterface $userTransformer)
    {
        $this->userRepository = $userRepository;
        $this->userTransformer = $userTransformer;
    }

    public function __invoke(RetrieveUserQuery $query, Deferred $deferred)
    {
        $user = $this->userRepository->byId(Id::fromUuid($query->getUserId()));
        $result = $this->userTransformer->transform($user);

        $deferred->resolve($result);
    }
}
