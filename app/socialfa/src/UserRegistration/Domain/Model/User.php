<?php
declare(strict_types=1);

namespace App\UserRegistration\Domain\Model;

use App\Core\Domain\ValueObject\Username;
use App\UserRegistration\Domain\Event\UserRegistered;
use App\Core\Domain\ValueObject\Email;
use App\Core\Domain\ValueObject\Name;
use App\Core\Domain\ValueObject\Id;
use DateTimeInterface;
use JsonSerializable;
use Prooph\EventSourcing\AggregateChanged;
use Prooph\EventSourcing\AggregateRoot;

class User extends AggregateRoot implements JsonSerializable
{
    /** @var Id */
    private $id;

    /** @var Username */
    private $username;

    /** @var Name */
    private $firstName;

    /** @var Name */
    private $lastName;

    /** @var Email */
    private $email;

    /** @var DateTimeInterface */
    private $birthDate;

    protected function __construct()
    {
    }

    public static function register(
        Id $id,
        Username $username,
        Name $firstName,
        Name $lastName,
        Email $email,
        DateTimeInterface $birthDate
    ): self {
        $registeredUser = new self();

        $registeredUser->recordThat(UserRegistered::occur(
            $id->asString(),
            [
                'id' => $id->asString(),
                'username' => $username->getUsername(),
                'firstName' => $firstName->getName(),
                'lastName' => $lastName->getName(),
                'email' => $email->getEmail(),
                'birthDate' => $birthDate->format('Y-m-d')
            ]
        ));

        return $registeredUser;
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public function getUsername(): Username
    {
        return $this->username;
    }

    public function getFirstName(): Name
    {
        return $this->firstName;
    }

    public function getLastName(): Name
    {
        return $this->lastName;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getBirthDate(): DateTimeInterface
    {
        return $this->birthDate;
    }

    protected function aggregateId(): string
    {
        return $this->id->asString();
    }

    protected function apply(AggregateChanged $event): void
    {
        switch (get_class($event)) {
            case UserRegistered::class:
                $this->applyUserRegistered($event);
        }
    }

    private function applyUserRegistered(UserRegistered $event): void
    {
        $this->id = $event->getId();
        $this->username = $event->getUsername();
        $this->firstName = $event->getFirstName();
        $this->lastName = $event->getLastName();
        $this->email = $event->getEmail();
        $this->birthDate = $event->getBirthDate();
    }

    /**
     * @return mixed|void
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id->asString(),
            'username' => $this->username->getUsername(),
            'firstName' => $this->firstName->getName(),
            'lastName' => $this->lastName->getName(),
            'email' => $this->email->getEmail(),
            'birthDate' => $this->birthDate->format('Y-m-d')
        ];
    }
}
