<?php
declare(strict_types=1);

namespace App\UserRegistration\Domain\Event;

use App\Core\Domain\ValueObject\Email;
use App\Core\Domain\ValueObject\Username;
use App\Core\Domain\ValueObject\Name;
use App\Core\Domain\ValueObject\Id;
use DateTimeImmutable;
use Prooph\EventSourcing\AggregateChanged;

class UserRegistered extends AggregateChanged
{
    public function getId(): Id
    {
        return Id::fromUuid($this->payload['id']);
    }

    public function getUsername(): Username
    {
        return Username::create($this->payload['username']);
    }

    public function getFirstName(): Name
    {
        return Name::fromWord($this->payload['firstName']);
    }

    public function getLastName(): Name
    {
        return Name::fromWord($this->payload['lastName']);
    }

    public function getBirthDate(): DateTimeImmutable
    {
        return \DateTimeImmutable::createFromFormat('Y-m-d', $this->payload['birthDate']);
    }

    public function getEmail(): Email
    {
        return Email::create($this->payload['email']);
    }
}
