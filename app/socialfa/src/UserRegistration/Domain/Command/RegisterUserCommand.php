<?php
declare(strict_types=1);

namespace App\UserRegistration\Domain\Command;

class RegisterUserCommand
{
    /** @var string */
    private $username;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $birthDate;

    /** @var string */
    private $email;

    /** @var string */
    private $userId;

    public function __construct(
        string $userId,
        string $username,
        string $firstName,
        string $lastName,
        string $birthDate,
        string $email
    ) {
        $this->userId = $userId;
        $this->username = $username;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->birthDate = $birthDate;
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getBirthDate(): string
    {
        return $this->birthDate;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}
