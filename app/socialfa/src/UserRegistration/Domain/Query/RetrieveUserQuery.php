<?php
declare(strict_types=1);

namespace App\UserRegistration\Domain\Query;

class RetrieveUserQuery
{
    /** @var string $userId */
    private $userId;

    public function __construct(string $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }
}
