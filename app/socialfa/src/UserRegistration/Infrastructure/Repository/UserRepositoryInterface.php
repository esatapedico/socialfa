<?php
declare(strict_types=1);

namespace App\UserRegistration\Infrastructure\Repository;

use App\Core\Domain\ValueObject\Id;
use App\UserRegistration\Domain\Model\User;

interface UserRepositoryInterface
{
    public function save(User $user): void;

    public function byId(Id $userId): User;
}
