<?php
declare(strict_types=1);

namespace App\UserRegistration\Infrastructure\Repository;

use App\Core\Domain\ValueObject\Id;
use App\UserRegistration\Domain\Model\User;
use Prooph\EventSourcing\Aggregate\AggregateRepository;

class EventStoreUserRepository extends AggregateRepository implements UserRepositoryInterface
{
    public function save(User $user): void
    {
        $this->saveAggregateRoot($user);
    }

    public function byId(Id $userId): User
    {
        return $this->getAggregateRoot($userId->asString());
    }
}
