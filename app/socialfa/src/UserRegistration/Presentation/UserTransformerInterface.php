<?php
declare(strict_types=1);

namespace App\UserRegistration\Presentation;

use App\UserRegistration\Domain\Model\User;

interface UserTransformerInterface
{
    /**
     * @param User $user
     *
     * @return mixed
     */
    public function transform(User $user);
}
