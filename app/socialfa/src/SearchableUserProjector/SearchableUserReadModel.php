<?php
declare(strict_types=1);

namespace App\SearchableUserProjector;

use App\Connections\Domain\Event\InvitationToConnectSent;
use App\UserRegistration\Domain\Event\UserRegistered;
use Elastica\Document;
use Elastica\Index;
use Prooph\EventStore\Projection\AbstractReadModel;

class SearchableUserReadModel extends AbstractReadModel
{
    /** @var Index */
    private $elasticSearchIndex;

    public function __construct(Index $elasticSearchIndex)
    {
        $this->elasticSearchIndex = $elasticSearchIndex;
    }

    public function init(): void
    {
        $this->elasticSearchIndex->create();
    }

    public function isInitialized(): bool
    {
        return $this->elasticSearchIndex->exists();
    }

    public function reset(): void
    {
        $this->elasticSearchIndex->create([], true);
    }

    public function delete(): void
    {
        $this->elasticSearchIndex->delete();
    }

    protected function onUserRegistered(UserRegistered $event)
    {
        $this->elasticSearchIndex->addDocuments([
            new Document(
                $event->aggregateId(),
                [
                    'username' => $event->getUsername()->getUsername(),
                    'firstName' => $event->getFirstName()->getName(),
                    'lastName' => $event->getLastName()->getName(),
                    'email' => $event->getEmail()->getEmail(),
                    'birthDate' => $event->getBirthDate()->format('Y-m-d'),
                    'connections' => []
                ],
                'user'
            )
        ]);
    }

    protected function onInvitationToConnectSent(InvitationToConnectSent $event)
    {
        $this->elasticSearchIndex->getClient()->updateDocument(
            $event->getInvitedUserId()->asString(),
            [
                'script' => [
                    'source' => 'ctx._source.connections.add(params.connection)',
                    'lang' => 'painless',
                    'params' => [
                        'connection' => $event->getInvitingUserId()->asString()
                    ]
                ]
            ],
            $this->elasticSearchIndex->getName(),
            'user'
        );

        $this->elasticSearchIndex->getClient()->updateDocument(
            $event->getInvitingUserId()->asString(),
            [
                'script' => [
                    'source' => 'ctx._source.connections.add(params.connection)',
                    'lang' => 'painless',
                    'params' => [
                        'connection' => $event->getInvitedUserId()->asString()
                    ]
                ]
            ],
            $this->elasticSearchIndex->getName(),
            'user'
        );
    }
}
