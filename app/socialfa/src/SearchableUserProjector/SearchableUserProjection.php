<?php
declare(strict_types=1);

namespace App\SearchableUserProjector;

use App\Connections\Domain\Event\InvitationToConnectSent;
use App\UserRegistration\Domain\Event\UserRegistered;
use Prooph\Bundle\EventStore\Projection\ReadModelProjection;
use Prooph\Common\Messaging\Message;
use Prooph\EventStore\Projection\ReadModelProjector;

class SearchableUserProjection implements ReadModelProjection
{
    public function project(ReadModelProjector $projector): ReadModelProjector
    {
        $readModel = $projector->readModel();

        $projector->reset();

        $projector->fromStream('event_stream')
            ->when(
                [
                    UserRegistered::class => function ($state, UserRegistered $event) use ($readModel) {
                        $readModel->stack('onUserRegistered', $event);
                    },
                    InvitationToConnectSent::class => function ($state, InvitationToConnectSent $event) use ($readModel) {
                        $readModel->stack('onInvitationToConnectSent', $event);
                    }
                ]
            )
        ;
        return $projector;
    }
}
