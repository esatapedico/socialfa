<?php
declare(strict_types=1);

namespace App\Core\Domain\Exception;

use DomainException;

class InvalidUsernameException extends DomainException
{
    public static function fromInvalidCharacters(string $username, string $expected): self
    {
        return new self("Username {$username} has invalid characters. Expected: {$expected}");
    }

    public static function tooShort(int $actual, int $expected): self
    {
        return new self("Username too short ({$actual} character(s)). Minimum: {$expected}");
    }

    public static function tooLong(int $actual, int $expected): self
    {
        return new self("Username too long ({$actual} character(s)). Maximum: {$expected}");
    }
}
