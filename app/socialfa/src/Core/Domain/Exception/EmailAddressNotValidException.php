<?php
declare(strict_types=1);

namespace App\Core\Domain\Exception;

use DomainException;

class EmailAddressNotValidException extends DomainException
{
}
