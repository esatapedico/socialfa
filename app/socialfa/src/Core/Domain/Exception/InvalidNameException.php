<?php
declare(strict_types=1);

namespace App\Core\Domain\Exception;

use DomainException;

class InvalidNameException extends DomainException
{
    public static function fromInvalidCharacters(string $name, string $expected): self
    {
        return new self("Name {$name} has invalid characters. Expected: {$expected}");
    }

    public static function tooShort(int $actual, int $expected): self
    {
        return new self("Name too short ({$actual} character(s)). Minimum: {$expected}");
    }

    public static function tooLong(int $actual, int $expected): self
    {
        return new self("Name too long ({$actual} character(s)). Maximum: {$expected}");
    }
}
