<?php
declare(strict_types=1);

namespace App\Core\Domain\ValueObject;

use App\Core\Domain\Exception\InvalidNameException;

class Name
{
    /** @var int */
    private const MINIMUM_LENGTH = 2;

    /** @var int */
    private const MAXIMUM_LENGTH = 100;

    /** @var string */
    private $name;

    private function __construct(string $name)
    {
        $this->name = $name;
    }

    public static function fromWord(string $word): self
    {
        $length = strlen($word);
        $sanitizedWord = filter_var($word, FILTER_SANITIZE_STRING);

        if ($length < self::MINIMUM_LENGTH) {
            throw InvalidNameException::tooShort($length, self::MINIMUM_LENGTH);
        }

        if ($length > self::MAXIMUM_LENGTH) {
            throw InvalidNameException::tooLong($length, self::MAXIMUM_LENGTH);
        }

        if ($sanitizedWord !== $word) {
            throw InvalidNameException::fromInvalidCharacters($word, $sanitizedWord);
        }

        return new self($word);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
