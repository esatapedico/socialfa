<?php
declare(strict_types=1);

namespace App\Core\Domain\ValueObject;

use App\Core\Domain\Exception\InvalidIdException;
use Ramsey\Uuid\Uuid;

class Id
{
    /** @var string */
    private $id;

    private function __construct(string $id)
    {
        $this->id = $id;
    }

    public static function next(): self
    {
        return new self(Uuid::uuid4()->toString());
    }

    public static function fromUuid(string $uuid): self
    {
        if (!Uuid::isValid($uuid)) {
            throw new InvalidIdException("UserId {$uuid} is not valid.");
        }

        return new self($uuid);
    }

    public function asString(): string
    {
        return $this->id;
    }
}
