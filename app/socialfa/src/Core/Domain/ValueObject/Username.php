<?php
declare(strict_types=1);

namespace App\Core\Domain\ValueObject;

use App\Core\Domain\Exception\InvalidUsernameException;

class Username
{
    /** @var int */
    private const MINIMUM_LENGTH = 3;

    /** @var int */
    private const MAXIMUM_LENGTH = 15;

    /** @var string */
    private $username;

    private function __construct(string $username)
    {
        $this->username = $username;
    }

    /**
     * @param string $username
     *
     * @return Username
     * @throws InvalidUsernameException
     */
    public static function create(string $username): self
    {
        $length = strlen($username);
        $sanitizedUsername = filter_var($username, FILTER_SANITIZE_STRING);

        if ($length < self::MINIMUM_LENGTH) {
            throw InvalidUsernameException::tooShort($length, self::MINIMUM_LENGTH);
        }

        if ($length > self::MAXIMUM_LENGTH) {
            throw InvalidUsernameException::tooLong($length, self::MAXIMUM_LENGTH);
        }

        if ($sanitizedUsername !== $username) {
            throw InvalidUsernameException::fromInvalidCharacters($username, $sanitizedUsername);
        }

        return new self($username);
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }
}
