<?php
declare(strict_types=1);

namespace App\Core\Domain\ValueObject;

use App\Core\Domain\Exception\EmailAddressNotValidException;

class Email
{
    /** @var string */
    private $email;

    private function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * @param string $email
     *
     * @return Email
     * @throws EmailAddressNotValidException
     */
    public static function create(string $email): self
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new EmailAddressNotValidException("E-mail '{$email}' is not valid.");
        }

        return new self($email);
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}
