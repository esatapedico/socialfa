<?php
declare(strict_types=1);

namespace App\Controller;

use App\Connections\Domain\Command\SendInvitationToConnectCommand;
use App\Connections\Domain\Query\RetrieveInvitationToConnectQuery;
use App\Core\Domain\ValueObject\Id;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class InvitationController
{
    /** @var CommandBus */
    private $commandBus;

    /** @var QueryBus */
    private $queryBus;

    public function __construct(CommandBus $commandBus, QueryBus $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    public function send(Request $request): Response
    {
        $payload = $request->getContent();
        $requestParams = json_decode($payload, true);

        if (false === $requestParams) {
            return new Response("Invalid request.", 400);
        }

        $invitationId = Id::next();

        $this->commandBus->dispatch(
            new SendInvitationToConnectCommand(
                $invitationId->asString(),
                $requestParams['invitingUserId'] ?? '',
                $requestParams['invitedUserId'] ?? '',
                $requestParams['message'] ?? ''
            )
        );

        return new Response("Created", 201, ['location' => "/invitation/{$invitationId->asString()}"]);
    }

    public function get(string $invitationId): JsonResponse
    {
        $response = new JsonResponse('Error', 500);

        $this->queryBus->dispatch(
            new RetrieveInvitationToConnectQuery($invitationId)
        )->then(function ($jsonInvitation) use ($response) {
            $response->setContent($jsonInvitation);
            $response->setStatusCode(200);
        });

        return $response;
    }
}
