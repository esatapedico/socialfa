<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class SocialfaController extends Controller
{
    public function show(): Response
    {
        return $this->render('app.html.twig');
    }
}
