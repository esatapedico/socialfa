<?php
declare(strict_types=1);

namespace App\Controller;

use App\Connections\Domain\Query\SearchAllUsersQuery;
use App\Connections\Domain\Query\SearchUserConnectionsQuery;
use App\UserRegistration\Domain\Query\RetrieveUserQuery;
use App\Connections\Domain\Query\RetrieveUserConnectionsQuery;
use App\Core\Domain\ValueObject\Id;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\UserRegistration\Domain\Command\RegisterUserCommand;

class UserController
{
    /** @var CommandBus */
    private $commandBus;

    /** @var QueryBus */
    private $queryBus;

    public function __construct(CommandBus $commandBus, QueryBus $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    public function register(Request $request): Response
    {
        $payload = $request->getContent();
        $requestParams = json_decode($payload, true);

        if (false === $requestParams) {
            return new Response("Invalid request.", 400);
        }

        $userId = Id::next();

        $this->commandBus->dispatch(
            new RegisterUserCommand(
                $userId->asString(),
                $requestParams['username'] ?? '',
                $requestParams['firstName'] ?? '',
                $requestParams['lastName'] ?? '',
                $requestParams['birthDate'] ?? '',
                $requestParams['email'] ?? ''
            )
        );

        return new Response("Created", 201, ['location' => "/user/{$userId->asString()}"]);
    }

    public function get(string $userId): JsonResponse
    {
        $response = new JsonResponse('Error', 500);

        $this->queryBus->dispatch(
            new RetrieveUserQuery($userId)
        )->then(function ($jsonUser) use ($response) {
            $response->setContent($jsonUser);
            $response->setStatusCode(200);
        });

        return $response;
    }

    public function getAll(string $searchTerms): JsonResponse
    {
        $response = new JsonResponse('Error', 500);

        $this->queryBus->dispatch(
            new SearchAllUsersQuery($searchTerms)
        )->then(function ($jsonUser) use ($response) {
            $response->setContent($jsonUser);
            $response->setStatusCode(200);
        });

        return $response;
    }

    public function getConnections(string $userId): JsonResponse
    {
        $response = new JsonResponse('Error', 500);

        $this->queryBus->dispatch(
            new RetrieveUserConnectionsQuery($userId)
        )->then(function ($jsonConnections) use ($response) {
            $response->setContent($jsonConnections);
            $response->setStatusCode(200);
        });

        return $response;
    }

    public function searchConnections(string $userId, string $searchTerms): JsonResponse
    {
        $response = new JsonResponse('Error', 500);

        $this->queryBus->dispatch(
            new SearchUserConnectionsQuery($userId, urldecode($searchTerms))
        )->then(function ($jsonConnections) use ($response) {
            $response->setContent($jsonConnections);
            $response->setStatusCode(200);
        });

        return $response;
    }
}
