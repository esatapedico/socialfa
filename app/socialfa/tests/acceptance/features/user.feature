Feature: Users

  Scenario: Register user
    Given I add "Content-Type" header equal to "application/json"
    When I send a POST request to "/user" with body:
      """
      {
        "username": "testuser",
        "firstName": "tester",
        "lastName": "testington",
        "birthDate": "1990-04-03",
        "email": "tester@testington.test"
      }
      """
    Then the response status code should be 201
