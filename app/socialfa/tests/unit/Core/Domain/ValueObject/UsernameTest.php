<?php

namespace Test\Core\Domain\ValueObject;

use App\Core\Domain\Exception\InvalidUsernameException;
use App\Core\Domain\ValueObject\Username;
use PHPUnit\Framework\TestCase;

class UsernameTest extends TestCase
{
    public function testCreateWillReturnUsername()
    {
        $username = Username::create('abc');
        $this->assertInstanceOf(Username::class, $username);
    }

    public function testGetUsernameWillReturnValidString()
    {
        $username = Username::create('abc');
        $this->assertTrue(is_string($username->getUsername()));
    }

    public function testCreateWillThrowExceptionWhenTooShort()
    {
        $this->expectException(InvalidUsernameException::class);
        $this->expectExceptionMessageRegExp('/too short/');
        Username::create('a');
    }

    public function testCreateWillThrowExceptionWhenTooLong()
    {
        $this->expectException(InvalidUsernameException::class);
        $this->expectExceptionMessageRegExp('/too long/');
        Username::create(str_pad('ab', 16, 'c'));
    }

    public function testCreateWillThrowExceptionWhenInvalidCharacters()
    {
        $this->expectException(InvalidUsernameException::class);
        $this->expectExceptionMessageRegExp('/invalid characters/');
        Username::create('abc#$\'');
    }
}
