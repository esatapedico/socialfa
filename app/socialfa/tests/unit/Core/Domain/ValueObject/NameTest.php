<?php

namespace Test\App\Core\Domain\ValueObject;

use App\Core\Domain\Exception\InvalidNameException;
use App\Core\Domain\ValueObject\Name;
use PHPUnit\Framework\TestCase;

class NameTest extends TestCase
{
    public function testFromWordWillReturnName()
    {
        $name = Name::fromWord('abc');
        $this->assertInstanceOf(Name::class, $name);
    }

    public function testGetNameWillReturnValidString()
    {
        $name = Name::fromWord('abc');
        $this->assertTrue(is_string($name->getName()));
    }

    public function testFromWordWillThrowExceptionWhenTooShort()
    {
        $this->expectException(InvalidNameException::class);
        $this->expectExceptionMessageRegExp('/too short/');
        Name::fromWord('a');
    }

    public function testFromWordWillThrowExceptionWhenTooLong()
    {
        $this->expectException(InvalidNameException::class);
        $this->expectExceptionMessageRegExp('/too long/');
        Name::fromWord(str_pad('ab', 101, 'c'));
    }

    public function testFromWordWillThrowExceptionWhenInvalidCharacters()
    {
        $this->expectException(InvalidNameException::class);
        $this->expectExceptionMessageRegExp('/invalid characters/');
        Name::fromWord('abc#$\'');
    }
}
