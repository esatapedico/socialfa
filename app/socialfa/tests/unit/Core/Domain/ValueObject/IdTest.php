<?php
declare(strict_types=1);

namespace Test\App\Core\Domain\ValueObject;

use App\Core\Domain\ValueObject\Id;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class IdTest extends TestCase
{
    public function testGeneratedIdWillReturnValidId()
    {
        $id = Id::next();
        $this->assertInstanceOf(Id::class, $id);
    }

    public function testGeneratedIdIsValidUuid()
    {
        $id = Id::next();
        $this->assertTrue(Uuid::isValid($id->asString()));
    }

    public function testIdFromValidUuidWillReturnValidId()
    {
        $id = Id::fromUuid('8918e54b-6a52-4177-b4a7-a68f9a12a9c4');
        $this->assertInstanceOf(Id::class, $id);
    }

    /**
     * @expectedException \App\Core\Domain\Exception\InvalidIdException
     */
    public function testIdFromUuidWillThrowExceptionWhenNotValid()
    {
        Id::fromUuid('123');
    }

    public function testIdAsStringWillReturnValidString()
    {
        $id = Id::next();
        $this->assertTrue(is_string($id->asString()));
    }
}
