<?php

namespace Test\Core\Domain\ValueObject;

use App\Core\Domain\Exception\EmailAddressNotValidException;
use App\Core\Domain\ValueObject\Email;
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    public function testCreateWillReturnEmail()
    {
        $email = Email::create('abc@a.com');
        $this->assertInstanceOf(Email::class, $email);
    }

    public function testGetEmailWillReturnValidString()
    {
        $email = Email::create('abc@a.com');
        $this->assertTrue(is_string($email->getEmail()));
    }

    public function testCreateWillThrowExceptionWhenInvalidCharacters()
    {
        $this->expectException(EmailAddressNotValidException::class);
        $this->expectExceptionMessageRegExp('/not valid/');
        Email::create('abc#$\'');
    }
}
