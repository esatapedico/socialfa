<?php

namespace Test\Connections\Domain\ValueObject;

use App\Connections\Domain\Exception\InvalidMessageException;
use App\Connections\Domain\ValueObject\Message;
use PHPUnit\Framework\TestCase;

class MessageTest extends TestCase
{
    public function testCreateFromTextWillReturnMessage()
    {
        $message = Message::createFromText('abc');
        $this->assertInstanceOf(Message::class, $message);
    }

    public function testGetMessageWillReturnValidString()
    {
        $message = Message::createFromText('abc');
        $this->assertTrue(is_string($message->getMessage()));
    }

    public function testCreateFromTextWillThrowExceptionWhenTooShort()
    {
        $this->expectException(InvalidMessageException::class);
        $this->expectExceptionMessageRegExp('/too short/');
        Message::createFromText('a');
    }

    public function testCreateFromTextWillThrowExceptionWhenTooLong()
    {
        $this->expectException(InvalidMessageException::class);
        $this->expectExceptionMessageRegExp('/too long/');
        Message::createFromText(str_pad('ab', 256, 'c'));
    }

    public function testCreateFromTextWillEscapeInvalidCharacters()
    {
        $message = Message::createFromText('a\'');
        $this->assertSame($message->getMessage(), 'a\\\'');
    }
}
