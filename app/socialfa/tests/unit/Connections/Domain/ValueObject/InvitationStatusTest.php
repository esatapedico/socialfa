<?php

namespace App\Connections\Domain\ValueObject;

use PHPUnit\Framework\TestCase;

class InvitationStatusTest extends TestCase
{
    public function testCreateWillReturnInvitationStatus()
    {
        $status = InvitationStatus::create(InvitationStatus::RECEIVED);
        $this->assertInstanceOf(InvitationStatus::class, $status);
    }

    /** @expectedException \App\Connections\Domain\Exception\InvalidInvitationStatusException */
    public function testCreateWillThrowExceptionWhenUnrecognizedString()
    {
        InvitationStatus::create('blabla');
    }

    public function testAsStringWillReturnValidString()
    {
        $status = InvitationStatus::create(InvitationStatus::SENT);
        $this->assertTrue(is_string($status->asString()));
    }
}
