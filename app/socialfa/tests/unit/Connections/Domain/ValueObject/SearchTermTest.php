<?php

namespace Test\Connections\Domain\ValueObject;

use App\Connections\Domain\Exception\InvalidSearchTermException;
use App\Connections\Domain\ValueObject\SearchTerm;
use PHPUnit\Framework\TestCase;

class SearchTermTest extends TestCase
{
    public function testFromStringWillReturnSearchTerm()
    {
        $searchTerm = SearchTerm::fromString('abc');
        $this->assertInstanceOf(SearchTerm::class, $searchTerm);
    }

    public function testGetSearchTermWillReturnValidString()
    {
        $searchTerm = SearchTerm::fromString('abc');
        $this->assertTrue(is_string($searchTerm->asString()));
    }

    public function testFromStringWillThrowExceptionWhenTooLong()
    {
        $this->expectException(InvalidSearchTermException::class);
        $this->expectExceptionMessageRegExp('/too long/');
        SearchTerm::fromString(str_pad('ab', 101, 'c'));
    }

    public function testCreateFromTextWillEscapeInvalidCharacters()
    {
        $searchTerm = SearchTerm::fromString('a\'');
        $this->assertSame($searchTerm->asString(), 'a&#39;');
    }
}
