<?php

namespace Test\Connections\Domain\Service;

use App\Connections\Domain\Command\SendInvitationToConnectCommand;
use App\Connections\Domain\Model\Invitation;
use App\Connections\Domain\Service\InviteToConnectService;
use App\Connections\Infrastructure\Repository\InvitationRepositoryInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;

class InviteToConnectServiceTest extends TestCase
{
    /** @var InviteToConnectService */
    private $inviteToConnectService;

    /** @var ObjectProphecy|InvitationRepositoryInterface */
    private $invitationRepository;

    public function setUp()
    {
        $this->invitationRepository = $this->prophesize(InvitationRepositoryInterface::class);
        $this->inviteToConnectService = new InviteToConnectService($this->invitationRepository->reveal());
    }

    public function test__invoke()
    {
        /** @var ObjectProphecy|SendInvitationToConnectCommand $command */
        $command = $this->prophesize(SendInvitationToConnectCommand::class);
        $command->getInvitingUserId()->willReturn('520896d9-a110-4d5d-9614-2d00f6bcc154');
        $command->getInvitedUserId()->willReturn('68fa631a-e35a-4465-9f44-4d1552e7dce7');
        $command->getInvitationId()->willReturn('26e4a500-55a8-4b39-b621-1d6a73748c20');
        $command->getMessage()->willReturn('message');

        ($this->inviteToConnectService)($command->reveal());

        $this->invitationRepository->save(Argument::type(Invitation::class))->shouldBeCalledTimes(1);
    }
}
