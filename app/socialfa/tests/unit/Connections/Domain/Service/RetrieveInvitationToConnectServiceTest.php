<?php

namespace Test\Connections\Domain\Service;

use App\Connections\Domain\Model\Invitation;
use App\Connections\Domain\Query\RetrieveInvitationToConnectQuery;
use App\Connections\Domain\Service\RetrieveInvitationToConnectService;
use App\Connections\Infrastructure\Repository\InvitationRepositoryInterface;
use App\Connections\Presentation\InvitationTransformerInterface;
use App\Core\Domain\ValueObject\Id;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;
use React\Promise\Deferred;

class RetrieveInvitationToConnectServiceTest extends TestCase
{
    /** @var RetrieveInvitationToConnectService */
    private $retrieveInvitationToConnectService;

    /** @var ObjectProphecy|InvitationRepositoryInterface */
    private $invitationRepository;

    /** @var ObjectProphecy|InvitationTransformerInterface */
    private $invitationTransformer;

    protected function setUp()
    {
        $this->invitationRepository = $this->prophesize(InvitationRepositoryInterface::class);
        $this->invitationTransformer = $this->prophesize(InvitationTransformerInterface::class);

        $this->retrieveInvitationToConnectService = new RetrieveInvitationToConnectService(
            $this->invitationRepository->reveal(),
            $this->invitationTransformer->reveal()
        );
    }


    public function test__invoke()
    {
        $query = $this->prophesize(RetrieveInvitationToConnectQuery::class);
        $deferred = $this->prophesize(Deferred::class);
        $invitation = $this->prophesize(Invitation::class)->reveal();

        $query->getInvitationId()->willReturn('fefc2d12-37d1-452d-a1e3-1a7420a5a1d9');

        $this->invitationRepository
            ->byId(Argument::type(Id::class))
            ->willReturn($invitation)
            ->shouldBeCalledTimes(1);

        $this->invitationTransformer->transform($invitation)->shouldBeCalledTimes(1);

        ($this->retrieveInvitationToConnectService)($query->reveal(), $deferred->reveal());
    }
}
