<?php

namespace App\Connections\Domain\Model;

use App\Core\Domain\ValueObject\Email;
use App\Core\Domain\ValueObject\Id;
use App\Core\Domain\ValueObject\Name;
use App\Core\Domain\ValueObject\Username;
use PHPUnit\Framework\TestCase;

class UserConnectionTest extends TestCase
{
    public function testJsonSerialize()
    {
        $userConnection = new UserConnection(
            Id::fromUuid('d19e91b8-0db9-4757-9f52-2ae320846917'),
            Username::create('user'),
            Name::fromWord('firstname'),
            Name::fromWord('lastname'),
            Email::create('a@bcd.ef'),
            \DateTimeImmutable::createFromFormat('Y-m-d', '2018-06-16')
        );

        $this->assertEquals(
            [
                'id' => 'd19e91b8-0db9-4757-9f52-2ae320846917',
                'username' =>'user',
                'firstName' => 'firstname',
                'lastName' => 'lastname',
                'email' => 'a@bcd.ef',
                'birthDate' => '2018-06-16'
            ],
            $userConnection->jsonSerialize()
        );
    }
}
