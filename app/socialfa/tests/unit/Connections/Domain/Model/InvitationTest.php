<?php

namespace Test\Connections\Domain\Model;

use App\Connections\Domain\Model\Invitation;
use App\Connections\Domain\ValueObject\InvitationStatus;
use App\Connections\Domain\ValueObject\Message;
use App\Core\Domain\ValueObject\Id;
use PHPUnit\Framework\TestCase;

class InvitationTest extends TestCase
{
    public function testSendWillCreateNewInvitationInstance()
    {
        $invitation = Invitation::send(
            Id::fromUuid('8918e54b-6a52-4177-b4a7-a68f9a12a9c4'),
            Id::fromUuid('f9a1b188-c3ac-44dc-9605-58cdeacb8058'),
            Id::fromUuid('4e767ba8-2ac5-41df-9d50-90f3d10972f9'),
            Message::createFromText('this is a message')
        );

        $this->assertInstanceOf(Invitation::class, $invitation);
    }

    public function testJsonEncodeWillReturnValidJson()
    {
        $invitation = Invitation::send(
            Id::fromUuid('8918e54b-6a52-4177-b4a7-a68f9a12a9c4'),
            Id::fromUuid('f9a1b188-c3ac-44dc-9605-58cdeacb8058'),
            Id::fromUuid('4e767ba8-2ac5-41df-9d50-90f3d10972f9'),
            Message::createFromText('this is a message')
        );

        $this->assertJson(json_encode($invitation));
    }


    public function testJsonSerializeWillReturnValidJson()
    {
        $invitation = Invitation::send(
            Id::fromUuid('8918e54b-6a52-4177-b4a7-a68f9a12a9c4'),
            Id::fromUuid('f9a1b188-c3ac-44dc-9605-58cdeacb8058'),
            Id::fromUuid('4e767ba8-2ac5-41df-9d50-90f3d10972f9'),
            Message::createFromText('this is a message')
        );

        $jsonArray = $invitation->jsonSerialize();

        $this->assertEquals(
            [
                'id' => '8918e54b-6a52-4177-b4a7-a68f9a12a9c4',
                'invitingUserId' => 'f9a1b188-c3ac-44dc-9605-58cdeacb8058',
                'invitedUserId' => '4e767ba8-2ac5-41df-9d50-90f3d10972f9',
                'message' => 'this is a message',
                'status' => InvitationStatus::SENT
            ],
            $jsonArray
        );
    }
}
