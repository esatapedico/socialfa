# Decision journal

### Choices made and implemented
- **PHP 7.2**: Latest version - no reason not to.
- **Apache**: I had a PHP+Apache docker image ready to go, so went with it because of time constraints. Would probably use nginx in real life because of better performance under most conditions.
- **Symfony 4.1**: Although it's not a LTS, it's already a minor version into the 4.0 milestone, and its improvements such as the easier API creation and message bus component fit the project goals quite well, including performance and limited time to build. The system is expected to outgrow millions of users, but as it starts being used by a smaller number of users first, updating Symfony to its next minor versions poses a minor risk until the next LTS finally arrives.
- **DDD**: This kind of application tends to grow giant with all kinds of different situations, so DDD will ensure that multiple teams can work on that, and the application can be split when needed and that everything won't go spaghetti.
- **Service bus + CQRS**: The command and query buses isolate the model from the infrastructure more easily, and alongside commands and queries turns easier to split the application into asynchronous service workers.
- **Event sourcing**: Because of the big ambitions of millions of users, it boosts performance by keeping read models clean and objective.
- **Prooph (Library)**: Simplifies implementing event sourcing and CQRS. It's well maintained and packed with many features. Might implement everything from scratch in the future the library overhead becomes an issue.
- **MySQL Event stream**: Would have chosen Kafka because it's reliable and scalable, but that's not supported out of the box by Prooph.
- **Elasticsearch read model for connections**: Because searches will be performed over and over on top of users and their connections, elasticsearch was the obvious choice for the read model of this part of the application.
- **PHPUnit/Prophecy + Behat for testing**: I'm familiar with them and it's easy to work out of the box with them.

### "Would have"s (if only there was more time...)
- **Kafka for service buses and event store**: And additional effort would have to be put to make it possible. Other service bus libraries support it out of the box, but they'd be slightly more difficult to integrate with the rest of the Prooph ecosystem. For event store, Prooph promises integration soon, but it wouldn't be that difficult to implement it myself (and maybe even contribute to the core project).
- **Microservice workers**: Although everything is in one single application, internally the bounded contexts are split with a few common elements. They can be easily separated into multiple applications that will take the commands and queries through a common bus and process asynchronously whatever is possible. The projectors would work isolated from the rest, read from the event store and populate the respective read models for each application.
- **Better error handling**: Because of time constraints, domain and infrastructure exceptions are bubbling up without proper context. The query promises were left without error handling as well (had to spend some time learning how to use those promises themselves), so they just return 500 Error when something goes wrong.
- **Authentication**: For obvious reasons. The current version is free for all.
- **User interface**: Unfortunately I had to leave this part out. I even started, but it was late already, and I'm happy with the other things I've accomplished. Besides, come on, just use a HTTP/REST client :).
- **Proper API documentation**: Swagger would be a good choice for easy documenting the endpoints and making an UI available for playing with it.
- **Better testing**: I covered the whole core and the models from the connections context. I wouldn't necessarily aim for 100% coverage, because that alone doesn't mean much, but I would cover all services models and more complex units. And all features with Behat, of course.
- **Environments/deployment**: A proper way of handling multiple environments and deploying. And even a deployed working version.
- **Proper git history**: I'd have developed everything in a more structured way, with proper commits on every step.
