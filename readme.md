# Socialfa

### Installation instructions

1. Make sure you have docker installed
2. Clone this repository
3. Bring stack up: `docker-compose up -d`
4. Install dependencies: `docker exec -it php-server composer install -d socialfa`

### Running
1. Run projector in separate terminal and leave it running: `docker exec -it php-server ./socialfa/bin/console event-store:projection:run searchable_user_projection`

### Endpoints
Use your favorite HTTP/REST client. There's no authentication implemented.

After creating a resource, its ID is available in the location in the response.
```
Register user:
  POST /user
  Payload example:
  {
    "username": "running",
    "firstName": "hahah",
    "lastName": "tome",
    "birthDate": "1987-01-21",
    "email": "testf@testingtona.testa"
  }

Search all users:
  GET /user/_search/{searchTerms{optional}}

Retrieve user:
  GET /user/{userId}
  
Get user connections:
  GET /user/{userId}/connections

Search user connections:
  GET /user/{userId}/connections/_search/{searchTerms{optional}}

Create invitation to connect:
  POST /invitation
  Payload example:
  {
      "invitingUserId": "b0a3f845-6da3-4a12-81dc-a1086e41fa5c",
      "invitedUserId": "9894c785-8543-4bb0-8a13-7457c98a79c1",
      "message": "Please connect with me!"
  }
  
Get connection invitation:
  GET /invitation/{invitationId}
```

### Decisions

See decision journal here: [Decision Journal](docs/decision-journal.md)
